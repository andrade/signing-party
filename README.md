
# Participando de uma Festa de Assinaturas de Chaves GPG (signing-party)

### Pré-requisitos
- Ter uma chave pública e privada válida na sua máquina configurada. (https://keyring.debian.org/creating-key.html)
- Dicas de configuração do arquivo de configuração do GnuPG
```bash
$ cat ~/.gnupg/gpg.conf
# Created by pEpEngine
keyserver keyserver.ubuntu.com
keyserver-options no-self-sigs-only
cert-digest-algo SHA512
no-emit-version 
no-comments 
personal-cipher-preferences AES AES256 AES192 CAST5
personal-digest-preferences SHA512 SHA384 SHA224
ignore-time-conflict 
allow-freeform-uid 
pinentry-mode loopback
```

### Instalando pacotes necessários - como root ou utilizar sudo
```bash
apt install signing-party msmtp msmtp-mta
```

### Configurando o serviço do msmtp
- O MSMTP é um cliente SMTP que permite enviar email pelo terminal Linux.
- Um exemplo do arquivo de configuração está disponível no seguinte arquivo: /usr/share/doc/msmtp/examples/msmtprc-user.example
- Como exemplo criaremos um arquivo configurando um cliente SMTP do gmail
```bash
vim ~/.msmtprc
# O conteúdo do arquivo deverá ser o seguinte:
# Set default values for all following accounts.
defaults
port 587
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile -
account gmail
host smtp.gmail.com
from <user>@gmail.com
auth on
user <user>@gmail.com
passwordeval gpg --for-your-eyes-only --quiet --decrypt ~/.msmtp-gmail.gpg
# Set a default account
account default : gmail
```

- Usaremos como exemplo o serviço do Gmail para envio de email e com isso teremos que criar uma chave para aplicativos a qual será utilizada pelo msmtp. A url para criação é a seguinte: https://myaccount.google.com/apppasswords
- Após criada a senha de aplicativos do Gmail iremos gerar um arquivo criptografado com sua chave gpg para que o msmtp possa ler e se autenticar toda vez que for enviar um email.
```bash
# O comando deverá ser executado dentro da pasta do usuário que está utilizando o serviço.(/home/user)
cd ~/
gpg --encrypt --output=.msmtp-gmail.gpg --recipient=<email_da_sua_chave_gpg_que_irá_criptografar> - <<END
copie-a-senha-criada-pelo-gmail
END
```
- Testando o envio de email
```bash
echo "teste de email" | msmtp -a default <email_do_destinatario>
# Após esse comando ser executado com sucesso deverá ser recebido um email como o corpo do texto:'teste de email' 
```

### Configurando o Caff
- O comando caff está presente no pacote signing-party empacotado no Debian
- Deverá ser alterado o arquivo ~/.caffrc (Altere os campos 'seu-nome', 'seu-email' e 'sua-fingerprint-da-chave-gpg' antes de copiar esse modelo)
```bash
# .caffrc -- vim:ft=perl:
# This file is in perl(1) format - see caff(1) for details.

$CONFIG{'owner'} = 'seu_nome';
$CONFIG{'email'} = 'seu-email';
#$CONFIG{'reply-to'} = 'foo@bla.org';

# You can get your long keyid from
#   gpg --keyid-format long --list-key <yourkeyid|name|emailaddress..>
#
# If you have a v4 key, it will simply be the last 16 digits of
# your fingerprint.
#
# Example:
#   $CONFIG{'keyid'} = [ qw{FEDCBA9876543210} ];
#  or, if you have more than one key:
#   $CONFIG{'keyid'} = [ qw{0123456789ABCDEF 89ABCDEF76543210} ];
$CONFIG{'keyid'} = [ qw{sua-fingerprint-da-chave-gpg} ];

# Select this/these keys to sign with
#$CONFIG{'local-user'} = [ qw{5A303591F8CDB08B E1DB53241AB7E528 B378B80629059B40 D83EC937563FB6D4 18827D0F11708D4C} ];

# Additionally encrypt messages for these keyids
#$CONFIG{'also-encrypt-to'} = [ qw{5A303591F8CDB08B E1DB53241AB7E528 B378B80629059B40 D83EC937563FB6D4 18827D0F11708D4C} ];

# Mail template to use for the encrypted part
#$CONFIG{'mail-template'} = << 'EOM';
#Hi,
#
#please find attached the user id{(scalar @uids >= 2 ? 's' : '')}
#{foreach $uid (@uids) {
#    $OUT .= "\t".$uid."\n";
#};}of your key {$key} signed by me.
#
#If you have multiple user ids, I sent the signature for each user id
#separately to that user id's associated email address. You can import
#the signatures by running each through `gpg --import`.
#
#Note that I did not upload your key to any keyservers. If you want this
#new signature to be available to others, please upload it yourself.
#With GnuPG this can be done using
#	gpg --keyserver keyserver.ubuntu.com  --send-key {$key}
#
#If you have any questions, don't hesitate to ask.
#
#Regards,
#-- 
#{$owner}
#EOM
```

- Após configurado o caff está pronto para o uso
```bash
# É possível assinar mais de uma chave por vez somente dar um espaço entre as fingerprints a serem assinadas na mesma linha.
caff -m yes <fingerprint da chave a ser assinada>
[NOTICE] Importing GnuPG options from ~/.gnupg/gpg.conf:
[NOTICE]     keyserver keyserver.ubuntu.com
[NOTICE]     keyserver-options no-self-sigs-only
[NOTICE]     cert-digest-algo SHA512
[NOTICE]     personal-cipher-preferences AES AES256 AES192 CAST5
[NOTICE]     personal-digest-preferences SHA512 SHA384 SHA224
[NOTICE]     pinentry-mode loopback
[INFO] Key XXXXXXXXXXXXXXX not changed
[NOTICE] Fetching keys from a keyserver (this may take a while)...
[INFO] Key XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX imported
[NOTICE] Sign the following keys according to your policy, then exit gpg with 'save' after signing each key

Really sign? (y/N) y

gpg> quit
Save changes? (y/N) y

[INFO] Key xxxxxxxxxxxxxxxxxxx UID 1 João Maria <joao@maria.wiki.br> done
Mail signature for 'seu nome' to 'joao@maria.wiki.br'? [Y/n] YES (from config/command line)
May 01 12:12:20 host=smtp.gmail.com tls=on auth=on user=email@gmail.com from=email@gmail.com recipients=joao@maria.wiki.br mailsize=5437 smtpstatus=250 smtpmsg='250 2.0.0 OK exitcode=EX_OK
[INFO] Key 0xAEDF123123123123 done

# Essa é a mensagem de sucesso!
```

### Referências
- https://wiki.debian.org/msmtp
- https://wiki.debian.org/Keysigning
- https://wiki.archlinux.org/title/Msmtp
- https://eriberto.pro.br/wiki/index.php?title=Usando_o_Caff_%2B_Exim_para_assinar_e_enviar_chaves_GPG